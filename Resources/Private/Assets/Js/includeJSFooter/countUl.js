/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Oliver Merz, Georg Kathan, Dominik Hilser - kitt3n.de
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/
var awesomeCountUl__isAnyPartOfElementInViewport = function(el) {
    var rect = el.getBoundingClientRect();
    // DOMRect { x: 8, y: 8, width: 100, height: 100, top: 8, right: 108, bottom: 108, left: 8 }
    var windowHeight = (window.innerHeight || document.documentElement.clientHeight);
    var windowWidth = (window.innerWidth || document.documentElement.clientWidth);

    // http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
    var vertInView = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
    var horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

    return [vertInView,horInView];
};

;(function() {
    var awesomeCountUl__throttle = function(type, name, obj) {
        var obj = obj || window;
        var running = false;
        var func = function() {
            if (running) { return; }
            running = true;
            requestAnimationFrame(function() {
                obj.dispatchEvent(new CustomEvent(name));
                running = false;
            });
        };
        obj.addEventListener(type, func);
    };
    awesomeCountUl__throttle ("scroll", "awesomeCountUl__optimizedScroll");
})();

var awesomeCountUls = [];
var awesomeCountUlsDataCountTo = [];
var awesomeCountUlsDataCountObject = [];
var awesomeCountUlsOnScreen = [];
var awesomeCountUl__interval = setInterval(function () {
    if (typeof kitt3n_windowLoad == 'undefined') {}  else {
        if (typeof kitt3n_windowLoad == 'boolean' && kitt3n_windowLoad) {
            clearInterval(awesomeCountUl__interval);

            if (kitt3n_applicationContext == "Development") {
                console.info('awesomeCountUl initialized');
            }

            awesomeCountUls = document.getElementsByClassName('awesome_count__to');
            window.addEventListener("awesomeCountUl__optimizedScroll", function() {
                if(awesomeCountUls.length > 0) {
                    var ii = 0;
                    for(var i = 0; i < awesomeCountUls.length; i++) {
                        awesomeCountUlsDataCountTo[i] = awesomeCountUls[i].getAttribute('data-count-to');
                        awesomeCountUlsDataCountObject[i] = awesomeCountUls[i].getAttribute('data-count-object');
                        awesomeCountUlsOnScreen[i] = awesomeCountUl__isAnyPartOfElementInViewport(awesomeCountUls[i]);
                        if(awesomeCountUlsOnScreen[i][0] && awesomeCountUlsOnScreen[i][1]) {
                            awesomeCountUls[i].style = "transition: top 2s ease;transition-delay: " + (ii*250) + "ms;top:" + awesomeCountUlsDataCountTo[i];
                            if (i > 0) {
                                if (awesomeCountUlsDataCountObject[i] == awesomeCountUlsDataCountObject[i-1]) {
                                    ii++;
                                } else {
                                    ii = 0;
                                }
                            } else {
                                ii++;
                            }
                        } else{
                            awesomeCountUls[i].style = "";
                        }
                    }
                }
            });
            if(awesomeCountUls.length > 0) {
                var ii = 0;
                for(var i = 0; i < awesomeCountUls.length; i++) {
                    awesomeCountUlsDataCountTo[i] = awesomeCountUls[i].getAttribute('data-count-to');
                    awesomeCountUlsDataCountObject[i] = awesomeCountUls[i].getAttribute('data-count-object');
                    awesomeCountUlsOnScreen[i] = awesomeCountUl__isAnyPartOfElementInViewport(awesomeCountUls[i]);
                    if(awesomeCountUlsOnScreen[i][0] && awesomeCountUlsOnScreen[i][1]) {
                        awesomeCountUls[i].style = "transition: top 2s ease;transition-delay: " + (ii*250) + "ms;top:" + awesomeCountUlsDataCountTo[i];
                        if (i > 0) {
                            if (awesomeCountUlsDataCountObject[i] == awesomeCountUlsDataCountObject[i-1]) {
                                ii++;
                            } else {
                                ii = 0;
                            }
                        } else {
                            ii++;
                        }
                    } else{
                        awesomeCountUls[i].style = "";
                    }
                }
            }
        }
    }
}, 250);