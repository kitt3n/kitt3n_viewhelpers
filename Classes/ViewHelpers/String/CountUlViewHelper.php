<?php
namespace KITT3N\Kitt3nViewhelpers\ViewHelpers\String;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Oliver Merz, Georg Kathan, Dominik Hilser - kitt3n.de
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

class CountUlViewHelper extends AbstractTagBasedViewHelper {

    /**
     * initialize arguments
     *
     * String :: string
     * String :: objectUid
     * Boolean :: inverse
     *
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerUniversalTagAttributes();
        $this->registerArgument('string', 'string', 'The numeric string to animate', true);
        $this->registerArgument('objectUid', 'string', 'Uid of the object', false);
        $this->registerArgument('inverse', 'boolean', 'Inverse counter', false);
    }

    /**
     * @return mixed
     *
     * Use e.g.:
     * <html xmlns:kitt3n="http://typo3.org/ns/KITT3N/Kitt3nViewhelpers/ViewHelpers">
     * ...
     * <kitt3n:countUl string="3+1"/>
     * <kitt3n:countUl string="1  " inverse="true"/>
     * <kitt3n:countUl string="1  " inverse="1"/>
     * ...
     */
    public function render() {

        $aNumeric = [' ','0','1','2','3','4','5','6','7','8','9'];
        $aMathemtic = ['*','/','-','+','>','<','=','°','∞'];

        $len = mb_strlen($this->arguments['string'], 'UTF-8');
        $aString = [];
        for ($i = 0; $i < $len; $i++) {
            $aString[] = mb_substr($this->arguments['string'], $i, 1, 'UTF-8');
        }

        $sHtml = '<span class="awesome_count">';

        foreach ($aString as $sString) {
            $randomId = rand(1000000000, 9999999999);
            $sCountFrom = '';
            $arraySearchNumeric = array_search($sString,$aNumeric);
            $arraySearchMathematic = array_search($sString,$aMathemtic);
            if($arraySearchNumeric !== false) {

                if ($this->arguments['inverse']) {
                    $sHtml .= '<style>ul.awesome_count__to#awesome_count__to--' . $randomId . ' {top:-' . ((count($aNumeric))*100) . '%}</style>';
                }

                $sHtml .= '<ul class="awesome_count__to" id="awesome_count__to--' . $randomId . '" data-count-to="-' . ($arraySearchNumeric*100) . '%" data-count-object="' . $this->arguments['objectUid'] . '" ' . $sCountFrom. '>';
                foreach ($aNumeric as $sNumeric) {
                    $sHtml .= '<li>' . ($sNumeric == ' ' ? '&nbsp;' : $sNumeric) . '</li>';
                }
                $sHtml .= '</ul>';

            }  elseif($arraySearchMathematic != false) {

                if ($this->arguments['inverse']) {
                    $sHtml .= '<style>ul#awesome_count__to--' . $randomId . ' {top:-' . ((count($aNumeric)-1)*100) . '%}</style>';
                }

                $sHtml .= '<ul class="awesome_count__to" id="awesome_count__to--' . $randomId . '" data-count-to="-' . ($arraySearchMathematic*100) . '%" data-count-object="' . $this->arguments['objectUid'] . '" ' . $sCountFrom. '>';
                foreach ($aMathemtic as $sMathematic) {
                    $sHtml .= '<li>' . $sMathematic . '</li>';
                }
                $sHtml .= '</ul>';
            } else {

                if ($this->arguments['inverse']) {
                    $sHtml .= '<style>ul#awesome_count__to--' . $randomId . ' {top:-100%}</style>';
                }

                $sHtml .= '<ul class="awesome_count__to"  id="awesome_count__to--' . $randomId . '" data-count-to="0%"  data-count-object="' . $this->arguments['objectUid'] . '" ' . $sCountFrom. '><li>' . $sString . '</li></ul>';
            }
        }

        $sHtml .= '</span>';

        return $sHtml;
    }
}