<?php
namespace KITT3N\Kitt3nViewhelpers\ViewHelpers\Date;


/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Oliver Merz, Georg Kathan, Dominik Hilser - kitt3n.de
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class DifferenceViewHelper extends AbstractViewHelper {

    /**
     * initialize arguments
     *
     * DateTime :: dStart
     * DateTime :: dEnd
     * String :: dReturnType (e.g. %y years | %m months | %a days | %h hours | %i minutes | %s seconds)
     * Boolean :: bAddCurrentDay
     *
     */
    public function initializeArguments()
    {
        $this->registerArgument('dStart', 'date', 'Start date', true);
        $this->registerArgument('dEnd', 'date', 'End date', true);
        $this->registerArgument('sReturnType', 'string', 'Format of the result', false);
        $this->registerArgument('bAddCurrentDay', 'boolean', 'Add a day to the result', false);
    }


    /**
     * @return mixed
     *
     *
     * Use e.g.:
     * <html xmlns:kitt3n="http://typo3.org/ns/KITT3N/Kitt3nViewhelpers/ViewHelpers">
     * or
     * {namespace kitt3n=KITT3N/Kitt3nViewhelpers/ViewHelpers}
     * ...
     * <v:variable.set name="startDate" value="{f:format.date(date: '17.01.1979', format: 'd.m.Y')}" />
     * <v:variable.set name="endDate" value="{f:format.date(date: '19.01.1979', format: 'd.m.Y')}" />
     *
     * <kitt3n:date.difference dStart="{startDate}" dEnd="{endDate}" dReturnType="%a" bAddCurrentDay="false" />
     * ...
     */
    public function render() {
        $dStart = $this->arguments['dStart'];
        $dEnd = $this->arguments['dEnd'];
        if ($this->arguments['sReturnType']) {
            $dReturnType = $this->arguments['sReturnType'];
        } else {
            $dReturnType = '%a';
        }

        // if dates are not in date format > cast them
        if(gettype($dStart) != 'object'){
            $dStart = new \DateTime($dStart);
        }
        if(gettype($dEnd) != 'object'){
            $dEnd = new \DateTime($dEnd);
        }

        // if start date is smaller than end date (valid order)
        if($dStart <= $dEnd){
            $oDatesDifference = $dStart->diff($dEnd);
            // + 1 (current day)
            if($this->arguments['bAddCurrentDay']){
                return $oDatesDifference->format($dReturnType) + 1;
            } else {
                return $oDatesDifference->format($dReturnType);
            }
        } else {
            // startDate is bigger than endDate
            return FALSE;
        }

    }
}