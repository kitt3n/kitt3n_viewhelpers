<?php
namespace KITT3N\Kitt3nViewhelpers\ViewHelpers\Format;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Oliver Merz, Georg Kathan, Dominik Hilser - kitt3n.de
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Extracts the title part from a common fluid link (e.g. 19 _blank - "testtitle with whitespace" &X=y)
 */
class ExtractTitleFromFluidLinkViewHelper extends AbstractViewHelper {

    /**
     * initialize arguments
     *
     * String :: link
     *
     */
    public function initializeArguments()
    {
        $this->registerArgument('link', 'string', 'Link from which the title should be extracted', true);
    }

    /**
     *
     * @return string
     *
     * Use e.g.:
     * <html xmlns:kitt3n="http://typo3.org/ns/KITT3N/Kitt3nViewhelpers/ViewHelpers">
     * or
     * {namespace kitt3n=KITT3N\Kitt3nViewhelpers\ViewHelpers}
     * ...
     * <kitt3n:format.extractTitleFromFluidLink link="{link}"/>
     * ...
     */
    public function render()
    {
        $sLink = $this->arguments['link'];

        // if the contains "" explode it
        if (strpos($sLink, '"') !== false) {

            // title tag is in between the ""
            $aLink = explode('"', $sLink);

            return $aLink[1];
        } else {
            if (strpos($sLink, ' - ') !== false) {
                // title tag at the end of the string after last -
                $aLink = explode(' - ', $sLink);
                if(count($aLink) > 1) {
                    return end($aLink);
                } else {
                    return FALSE;
                }
            }
        }
    }

}