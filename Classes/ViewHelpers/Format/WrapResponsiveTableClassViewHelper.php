<?php
namespace KITT3N\Kitt3nViewhelpers\ViewHelpers\Format;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Oliver Merz, Georg Kathan, Dominik Hilser - kitt3n.de
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Wrap table with a div tag which has the class "table-responsive"
 */
class WrapResponsiveTableClassViewHelper extends AbstractViewHelper {

    /**
     * initialize arguments
     *
     * String :: bodytext
     *
     */
    public function initializeArguments()
    {
        $this->registerArgument('bodytext', 'string', 'Text in which the tables should be wrapped with a div tag with the class responsive-table.', true);
    }

    /**
     *
     * @return string
     *
     * Use e.g.:
     * <html xmlns:kitt3n="http://typo3.org/ns/KITT3N/Kitt3nViewhelpers/ViewHelpers">
     * or
     * {namespace kitt3n=KITT3N\Kitt3nViewhelpers\ViewHelpers}
     * ...
     * <kitt3n:format.wrapResponsiveTableClass bodytext="{data.bodytext}"/>
     * ...
     */
    public function render()
    {
        $sBodytext = $this->arguments['bodytext'];

        // replace opening tags
        if (strpos($this->arguments['bodytext'], '<table>') !== false) {
            $sBodytext = str_replace(
                '<table>',
                '<div class="table-responsive"><table>',
                $sBodytext
            );
        }
        if (strpos($this->arguments['bodytext'], '<table class="contenttable">') !== false) {
            $sBodytext = str_replace(
                '<table class="contenttable">',
                '<div class="table-responsive"><table class="contenttable">',
                $sBodytext
            );
        }

        // replace closing tags
        if (strpos($this->arguments['bodytext'], '</table>') !== false) {
            $sBodytext = str_replace(
                '</table>',
                '</table></div>',
                $sBodytext
            );

        }

        return $sBodytext;
    }

}